FactoryBot.define do
  factory :user, class: "User" do
    name                     { "Hyuma" }
    email                    { "tester@example.com" }
    password                 { "tester" }
    password_confirmation    { "tester" }
  end
end
