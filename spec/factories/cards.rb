FactoryBot.define do
  factory :card, class: "Card" do
    title   { "card title" }
    memo    { "memo" }
    list    { nil }
  end
end
