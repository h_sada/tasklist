FactoryBot.define do
  factory :list, class: "List" do
    title   { "list" }
    user    { nil }
  end

  factory :lists, class: "List" do
    sequence(:title) { |n| "list#{n}" }
    user { nil }
  end
end
