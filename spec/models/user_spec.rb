require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { create(:user) }

  it "is valid with user" do
    expect(user.valid?).to be_truthy
  end

  describe "confirm users' validation" do
    context "about 1 user" do
      it "is invalid with a name" do
        user.name = ""
        expect(user.valid?).to be_falsey
      end

      it "is invalid with a email" do
        user.email = ""
        expect(user.valid?).to be_falsey
      end

      it "is invalid with a empty password" do
        user.password = ""
        expect(user.valid?).to be_falsey
      end

      it "is valid with a name with 19 characters" do
        user.name = "a" * 20
        expect(user.valid?).to be_truthy
      end

      it "is invalid with a name with 20 characters" do
        user.name = "a" * 21
        expect(user.valid?).to be_falsey
      end

      it "is invalid when password differs from password_confirmation" do
        user.password = "tomorrow"
        expect(user.valid?).to be_falsey
      end
    end
  end

  context "2 users or more" do
    it "is invalid with the same email" do
      another_user = user.dup
      expect(another_user.valid?).to be_falsey
    end
  end
end
