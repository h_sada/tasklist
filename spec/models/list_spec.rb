require 'rails_helper'

RSpec.describe List, type: :model do
  let(:user)      { create(:user) }
  let(:list)      { create(:list, user: user) }

  it "is valid with a list" do
    expect(list.valid?).to be_truthy
  end

  describe "confirm lists validation" do
    context "about 1 list" do
      it "is invalid with a empty title" do
        list.title = ""
        expect(list.valid?).to be_falsey
      end

      it "is valid with a title in 20 characters" do
        list.title = "a" * 20
        expect(list.valid?).to be_truthy
      end

      it "is valid with a title in 21 characters" do
        list.title = "a" * 21
        expect(list.valid?).to be_falsey
      end

      it "is invalid without a user" do
        list.user = nil
        expect(list.valid?).to be_falsey
      end
    end

    context "2 lists or more" do
      it "could make 2 lists or more with the same title" do
        another_list = list.dup
        expect(another_list.valid?).to be_truthy
      end
    end
  end
end
