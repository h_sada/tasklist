require 'rails_helper'

RSpec.describe Card, type: :model do
  let(:user)      { create(:user) }
  let(:list)      { create(:list, user: user) }
  let(:card)      { create(:card, list: list) }

  it "is valid with a card" do
    expect(card.valid?).to be_truthy
  end

  describe "confirm cards validation" do
    context "about 1 card" do
      it "is in valid with empty title" do
        card.title = ""
        expect(card.valid?).to be_falsey
      end

      it "is valid with a title in 20 characters" do
        card.title = "a" * 20
        expect(card.valid?).to be_truthy
      end

      it "is valid with a title in 21 characters" do
        card.title = "a" * 21
        expect(card.valid?).to be_falsey
      end

      it "is invalid without a list" do
        card.list = nil
        expect(card.valid?).to be_falsey
      end
    end

    context "2 cards or more" do
      it "could make 2 cards or more with the same title" do
        another_card = card.dup
        another_card.memo = "another memo"
        expect(another_card.valid?).to be_truthy
      end
    end
  end
end
