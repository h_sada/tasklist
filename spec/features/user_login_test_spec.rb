require 'rails_helper'

RSpec.feature "UserLoginTests", type: :feature do
  feature "login and logou test", js: true do
    given(:user)            { create(:user) }
    given(:valid_params)    { attributes_for(:user) }
    given(:invalid_params)  { attributes_for(:user, email: "fdsafdfda") }
    given(:login_with_params) do
      visit new_user_session_path
      fill_in "Email", with: user.email
      fill_in "Password", with: user.password
      click_on "Login"
    end

    scenario "login with valid parameters" do
      login_with_params
      expect(current_path).to eq root_path
      expect(page).to have_content user.name
    end

    scenario "login with invalid parameters" do
      visit new_user_session_path
      fill_in "Email", with: "invalid"
      fill_in "Password", with: user.password
      click_on "Login"
      expect(current_path).to eq new_user_session_path
      expect(page).to have_content "Login"
    end

    scenario "logout correctly" do
      login_with_params
      expect(current_path).to eq root_path
      click_on "Logout"
      expect(current_path).to eq new_user_session_path
      expect(page).to have_content "Login"
    end
  end
end
