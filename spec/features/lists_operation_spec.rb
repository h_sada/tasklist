require 'rails_helper'

RSpec.feature "ListsOperation", type: :feature do
  feature "create and destroy lists", js: true do
    given(:user)     { create(:user) }
    given(:lists)    { create_list(:lists, 3, user: user) }

    background do
      sign_in(user)
      user.lists << lists
    end

    context "shows lists correctly" do
      scenario "has just 3 lists" do
        visit root_path
        expect(current_path).to eq root_path
        expect(page).to have_selector "div.lists", count: lists.count
      end

      scenario "NOT have 4 lists or more" do
        visit root_path
        expect(current_path).to eq root_path
        expect(page).not_to have_selector "div.lists", count: (lists.count + 1)
      end
    end

    context "#create" do
      scenario "creates a list" do
        visit root_path
        expect(current_path).to eq root_path
        click_on "Create List"
        expect(current_path).to eq new_list_path
        find(".form-control").set("")
        expect do
          find(".submitBtn").click
          sleep 0.3
        end.not_to change(List, :count)
        expect(current_path).to eq new_list_path
      end

      scenario "creates no list" do
        visit root_path
        expect(current_path).to eq root_path
        click_on "Create List"
        expect(current_path).to eq new_list_path
        find(".form-control").set("CREATED")
        expect do
          find(".submitBtn").click
          sleep 0.3
        end.to change(List, :count).by(1)
        expect(current_path).to eq root_path
        expect(page).to have_selector "h2", text: "CREATED"
        expect(page).to have_selector "div.lists", count: (lists.count + 1)
      end
    end

    context "#edit" do
      scenario "edits a list" do
        visit root_path
        first(".fa-pen").click
        expect(current_path).to eq edit_list_path(lists.first.id)
        find(".form-control").set("UPDATED")
        find(".submitBtn").click
        sleep 0.3
        expect(current_path).to eq root_path
        expect(page).to have_selector "h2", text: "UPDATED"
        expect(page).to have_selector "div.lists", count: lists.count
      end

      scenario "edits no list" do
        visit root_path
        first(".fa-pen").click
        expect(current_path).to eq edit_list_path(lists.first.id)
        find(".form-control").set("")
        find(".submitBtn").click
        sleep 0.3
        expect(current_path).to eq edit_list_path(lists.first.id)
      end
    end

    context "#delete" do
      scenario "deletes a list" do
        visit root_path
        first(".fa-trash").click
        page.accept_confirm "Are you sure to delete?"
        expect(current_path).to eq root_path
        expect(page).to have_selector "div.lists", count: (lists.count - 1)
      end

      scenario "deletes no list" do
        visit root_path
        first(".fa-trash").click
        page.dismiss_confirm "Are you sure to delete?"
        expect(current_path).to eq root_path
        expect(page).to have_selector "div.lists", count: lists.count
      end
    end
  end
end
