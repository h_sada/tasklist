require 'rails_helper'

RSpec.feature "SiteLayouts", type: :feature do
  feature "site layouts test", js: true do
    let(:user) { create(:user) }

    background do
      sign_in(user)
    end

    context "check header links" do
      scenario "click user name" do
        visit root_path
        click_on user.name
        expect(current_path).to eq edit_user_registration_path
      end

      scenario "click header title" do
        visit root_path
        click_on "#{user.name}'s Tasklist"
        expect(current_path).to eq root_path
      end

      scenario "click Create List" do
        visit root_path
        click_on "Create List"
        expect(current_path).to eq new_list_path
      end

      scenario "click Logout" do
        visit root_path
        click_on "Logout"
        expect(current_path).to eq new_user_session_path
      end
    end
  end
end
