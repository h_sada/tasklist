require 'rails_helper'

RSpec.describe "UserResponses", type: :request do
  let(:user)   { create(:user) }
  let(:valid_params) do
    attributes_for(:user, email: "updated@example.com", current_password: "tester")
  end
  let(:invalid_params) do
    attributes_for(:user, email: "fsalk3r3kfda", current_password: "tester")
  end

  describe "get responds" do
    context "as a login user" do
      before do
        sign_in(user)
      end

      describe "#new" do
        it "responds [200]" do
          get new_user_registration_path
          expect(response).not_to have_http_status 200
        end
        it "responds [:success]" do
          get new_user_registration_path
          expect(response).not_to have_http_status :success
        end
        it "redirects to root_path" do
          get new_user_registration_path
          expect(response).to redirect_to root_path
        end
      end

      describe "#create" do
        it "responds [302]" do
          post user_registration_path, params: { user: valid_params }
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          post user_registration_path, params: { user: valid_params }
          expect(response).to have_http_status :found
        end
        it "creates no user" do
          expect do
            post user_registration_path, params: { user: valid_params }
          end.not_to change(User, :count)
        end
        it "redirects to root_path" do
          post user_registration_path, params: { user: valid_params }
          expect(response).to redirect_to root_path
        end
      end

      describe "#edit" do
        it "responds [200]" do
          get edit_user_registration_path(user)
          expect(response).to have_http_status 200
        end
        it "responds [:success]" do
          get edit_user_registration_path(user)
          expect(response).to have_http_status :success
        end
        it "shows the user email" do
          get edit_user_registration_path(user)
          expect(response.body).to include user.email
        end
      end

      describe "#update" do
        context "with valid parameters" do
          it "responds [302]" do
            put user_registration_path, params: { user: valid_params }
            expect(response).to have_http_status 302
          end
          it "responds [:found]" do
            put user_registration_path, params: { user: valid_params }
            expect(response).to have_http_status :found
          end
          it "updates a user" do
            expect do
              put user_registration_path, params: { user: valid_params }
            end.to change { User.find(user.id).email }.from(user.email).to("updated@example.com")
          end
          it "redirects to root_path" do
            put user_registration_path, params: { user: valid_params }
            expect(response).to redirect_to root_path
          end
        end

        context "with invalid parameters" do
          it "responds [200]" do
            put user_registration_path, params: { user: invalid_params }
            expect(response).to have_http_status 200
          end
          it "responds [:success]" do
            put user_registration_path, params: { user: invalid_params }
            expect(response).to have_http_status :success
          end
          it "updates no user" do
            expect do
              put user_registration_path, params: { user: invalid_params }
            end.not_to change { User.find(user.id).email }
          end
        end
      end

      describe "#destroy" do
        it "responds [302]" do
          delete user_registration_path
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          delete user_registration_path
          expect(response).to have_http_status :found
        end
        it "deletes a user" do
          expect do
            delete user_registration_path
          end.to change(User, :count).by(-1)
        end
        it "redirects to root_path" do
          delete user_registration_path
          expect(response).to redirect_to root_path
        end
      end
    end

    context "as a guest" do
      describe "#new" do
        it "responds [302]" do
          get new_user_registration_path
          expect(response).not_to have_http_status 302
        end
        it "responds [:found]" do
          get new_user_registration_path
          expect(response).not_to have_http_status :found
        end
      end

      describe "#create" do
        it "responds [302]" do
          post user_registration_path, params: { user: valid_params }
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          post user_registration_path, params: { user: valid_params }
          expect(response).to have_http_status :found
        end
        it "creates a user" do
          expect do
            post user_registration_path, params: { user: valid_params }
          end.to change(User, :count).by(1)
        end
        it "redirects to root_path" do
          post user_registration_path, params: { user: valid_params }
          expect(response).to redirect_to root_path
        end
      end

      describe "#edit" do
        it "responds [401]" do
          get edit_user_registration_path(user)
          expect(response).to have_http_status 401
        end
        it "responds [:unauthorized]" do
          get edit_user_registration_path(user)
          expect(response).to have_http_status :unauthorized
        end
        it "shows user email" do
          get edit_user_registration_path(user)
          expect(response.body).not_to include user.email
        end
      end

      describe "#update" do
        it "responds [302]" do
          put user_registration_path, params: { user: valid_params }
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          put user_registration_path, params: { user: valid_params }
          expect(response).to have_http_status :found
        end
        it "updates no user" do
          expect do
            put user_registration_path, params: { user: valid_params }
          end.not_to change { User.find(user.id) }
        end
        it "redirects to root_path" do
          put user_registration_path, params: { user: valid_params }
          expect(response).to redirect_to new_user_session_path
        end
      end

      describe "#destroy" do
        it "responds [302]" do
          delete user_registration_path
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          delete user_registration_path
          expect(response).to have_http_status :found
        end
        it "deletes no user" do
          expect do
            delete user_registration_path
          end.not_to change(User, :count)
        end
        it "redirects to sign_in" do
          delete user_registration_path
          expect(response).to redirect_to new_user_session_path
        end
      end
    end
  end
end
