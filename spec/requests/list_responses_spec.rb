require 'rails_helper'

RSpec.describe "ListResponses", type: :request do
  describe "list responses" do
    let(:user)            { create(:user) }
    let(:list)            { create(:list, user: user) }
    let(:valid_params)    { attributes_for(:list, title: "updated") }
    let(:invalid_params)  { attributes_for(:list, title: "a" * 30) }

    context "as a login user" do
      before do
        sign_in(user)
      end

      describe "#new" do
        it "responds [200]" do
          get new_list_path
          expect(response).to have_http_status 200
        end
        it "responds [:success]" do
          get new_list_path
          expect(response).to have_http_status :success
        end
      end

      describe "#create" do
        it "responds [302]" do
          post lists_path, params: { list: valid_params }
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          post lists_path, params: { list: valid_params }
          expect(response).to have_http_status :found
        end
        it "creates a list" do
          expect do
            post lists_path, params: { list: valid_params }
          end.to change(List, :count).by(1)
        end
        it "redirects to #edit" do
          post lists_path, params: { list: valid_params }
          expect(response).to redirect_to root_path
        end
      end

      describe "#edit" do
        it "responds [200]" do
          get edit_list_path(list)
          expect(response).to have_http_status 200
        end
        it "responds [:success]" do
          get edit_list_path(list)
          expect(response).to have_http_status :success
        end
        it "shows the list title" do
          get edit_list_path(list)
          expect(response.body).to include list.title
        end
      end

      describe "#update" do
        context "with a valid parameters" do
          it "responds [302]" do
            put list_path(list), params: { list: valid_params }
            expect(response).to have_http_status 302
          end
          it "responds [:found]" do
            put list_path(list), params: { list: valid_params }
            expect(response).to have_http_status :found
          end
          it "updates a list" do
            expect do
              put list_path(list), params: { list: valid_params }
            end.to change { List.find(list.id).title }
          end
          it "redirects to root_path" do
            put list_path(list), params: { list: valid_params }
            expect(response).to redirect_to root_path
          end
        end

        context "with a invalid paramteres" do
          it "responds [200]" do
            put list_path(list), params: { list: invalid_params }
            expect(response).to have_http_status 200
          end
          it "responds [:success]" do
            put list_path(list), params: { list: invalid_params }
            expect(response).to have_http_status :success
          end
          it "updates no list" do
            expect do
              put list_path(list), params: { list: invalid_params }
            end.not_to change { List.find(list.id).title }
          end
        end
      end

      describe "#destroy" do
        it "responds [302]" do
          delete list_path(list)
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          delete list_path(list)
          expect(response).to have_http_status :found
        end
        it "destroys a list" do
          post lists_path(list), params: { list: valid_params }
          expect do
            delete list_path(list)
          end.to change(List, :count).by(-1)
        end
      end
    end

    context "as a guest" do
      describe "#new" do
        it "responds [302]" do
          get new_list_path
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          get new_list_path
          expect(response).to have_http_status :found
        end
        it "redirects to sign_in" do
          get new_list_path
          expect(response).to redirect_to new_user_session_path
        end
      end

      describe "#create" do
        it "responds [302]" do
          post lists_path, params: { list: valid_params }
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          post lists_path, params: { list: valid_params }
          expect(response).to have_http_status :found
        end
        it "creates no list" do
          expect do
            post lists_path, params: { list: valid_params }
          end.not_to change(List, :count)
        end
        it "redirects to sign_in" do
          get new_list_path
          expect(response).to redirect_to new_user_session_path
        end
      end

      describe "#edit" do
        it "responds [302]" do
          get edit_list_path(list)
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          get edit_list_path(list)
          expect(response).to have_http_status :found
        end
        it "redirects to sign_in" do
          get edit_list_path(list)
          expect(response).to redirect_to new_user_session_path
        end
      end

      describe "#update" do
        it "responds [302]" do
          put list_path(list), params: { list: valid_params }
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          put list_path(list), params: { list: valid_params }
          expect(response).to have_http_status :found
        end
        it "updates no list" do
          expect do
            put list_path(list), params: { list: valid_params }
          end.not_to change { List.find(list.id).title }
        end
        it "redirects to sign_in" do
          put list_path(list), params: { list: valid_params }
          expect(response).to redirect_to new_user_session_path
        end
      end

      describe "#destroy" do
        it "responds [302]" do
          delete list_path(list)
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          delete list_path(list)
          expect(response).to have_http_status :found
        end
        it "destroys a list" do
          post lists_path(list), params: { list: valid_params }
          expect do
            delete list_path(list)
          end.not_to change(List, :count)
        end
        it "redirects to sign_in" do
          delete list_path(list)
          expect(response).to redirect_to new_user_session_path
        end
      end
    end
  end
end
