require 'rails_helper'

RSpec.describe "CardsResponses", type: :request do
  describe "cards responses" do
    let(:user)        { create(:user) }
    let(:list)        { create(:list, user: user) }
    let!(:card) { create(:card, list: list) }
    let(:valid_params)    { attributes_for(:card, list_id: list.id, title: "PARAMETER's") }
    let(:invalid_params)  { attributes_for(:card, list_id: list.id, title: "") }

    context "as a login user" do
      before do
        sign_in(user)
      end

      describe "#new" do
        it "responds [200]" do
          get new_list_card_path(card)
          expect(response).to have_http_status 200
        end
        it "responds [:success]" do
          get new_list_card_path(card)
          expect(response).to have_http_status :success
        end
      end

      describe "#create" do
        it "responds [302]" do
          post list_cards_path(card), params: { card: valid_params }
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          post list_cards_path(card), params: { card: valid_params }
          expect(response).to have_http_status :found
        end
        it "creates a card" do
          expect do
            post list_cards_path(card), params: { card: valid_params }
          end.to change(Card, :count).by(1)
        end
        it "redirects to root_path" do
          post list_cards_path(card), params: { card: valid_params }
          expect(response).to redirect_to root_path
        end
      end

      describe "#show" do
        it "responds [200]" do
          get list_card_path(list, card)
          expect(response).to have_http_status 200
        end
        it "responds [:success]" do
          get list_card_path(list, card)
          expect(response).to have_http_status :success
        end
        it "shows the card title" do
          get list_card_path(list, card)
          expect(response.body).to include card.title
        end
        it "shows the card memo" do
          get list_card_path(list, card)
          expect(response.body).to include card.memo
        end
      end

      describe "#edit" do
        it "responds [200]" do
          get edit_list_card_path(list, card)
          expect(response).to have_http_status 200
        end
        it "responds [:success]" do
          get edit_list_card_path(list, card)
          expect(response).to have_http_status :success
        end
        it "shows the card detail" do
          get list_card_path(list, card)
          expect(response.body).to include card.title
        end
        it "shows the card memo" do
          get list_card_path(list, card)
          expect(response.body).to include card.memo
        end
      end

      describe "#update" do
        context "with a valid parameters" do
          it "responds [302]" do
            put list_card_path(list, card), params: { card: valid_params }
            expect(response).to have_http_status 302
          end
          it "responds [:found]" do
            put list_card_path(list, card), params: { card: valid_params }
            expect(response).to have_http_status :found
          end
          it "updates a card" do
            expect do
              put list_card_path(list, card), params: { card: valid_params }
            end.to change { Card.find(card.id).title }
          end
          it "redirects to root_path" do
            put list_card_path(list, card), params: { card: valid_params }
            expect(response).to redirect_to root_path
          end
        end

        context "with a invalid parameters" do
          it "responds [200]" do
            put list_card_path(list, card), params: { card: invalid_params }
            expect(response).to have_http_status 200
          end
          it "responds [:success]" do
            put list_card_path(list, card), params: { card: invalid_params }
            expect(response).to have_http_status :success
          end
          it "updates no card" do
            expect do
              put list_card_path(list, card), params: { card: invalid_params }
            end.not_to change { Card.find(card.id).title }
          end
        end
      end

      describe "#destroy" do
        it "responds [302]" do
          delete list_card_path(list, card)
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          delete list_card_path(list, card)
          expect(response).to have_http_status :found
        end
        it "deletes a card" do
          post list_cards_path(card), params: { card: valid_params }
          expect do
            delete list_card_path(list, card)
          end.to change(Card, :count).by(-1)
        end
        it "redirects to root_path" do
          post list_cards_path(card), params: { card: valid_params }
          delete list_card_path(list, card), params: { card: valid_params }
          expect(response).to redirect_to root_path
        end
      end
    end

    context "as a guest" do
      describe "#new" do
        it "responds [302]" do
          get new_list_card_path(list)
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          get new_list_card_path(list)
          expect(response).to have_http_status :found
        end
        it "redirects to sign_in" do
          get new_list_card_path(list)
          expect(response).to redirect_to new_user_session_path
        end
      end

      describe "#create" do
        it "responds [302]" do
          post list_cards_path(card), params: { card: valid_params }
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          post list_cards_path(card), params: { card: valid_params }
          expect(response).to have_http_status :found
        end
        it "creates no card" do
          expect do
            post list_cards_path(card), params: { card: valid_params }
          end.not_to change(Card, :count)
        end
        it "redirects to sign_in" do
          post list_cards_path(card), params: { card: valid_params }
          expect(response).to redirect_to new_user_session_path
        end
      end

      describe "#show" do
        it "responds [302]" do
          get list_card_path(list, card)
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          get list_card_path(list, card)
          expect(response).to have_http_status :found
        end
        it "redirects to sign_in" do
          get list_card_path(list, card)
          expect(response).to redirect_to new_user_session_path
        end
      end

      describe "#edit" do
        it "responds [302]" do
          get edit_list_card_path(list, card)
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          get edit_list_card_path(list, card)
          expect(response).to have_http_status :found
        end
        it "redirects to sign_in" do
          get edit_list_card_path(list, card)
          expect(response).to redirect_to new_user_session_path
        end
      end

      describe "#update" do
        context "with a valid parameters" do
          it "responds [302]" do
            put list_card_path(list, card), params: { card: valid_params }
            expect(response).to have_http_status 302
          end
          it "responds [:found]" do
            put list_card_path(list, card), params: { card: valid_params }
            expect(response).to have_http_status :found
          end
          it "updates no card" do
            expect do
              put list_card_path(list, card), params: { card: valid_params }
            end.not_to change { Card.find(card.id).title }
          end
          it "redirects to sign_in" do
            put list_card_path(list, card), params: { card: valid_params }
            expect(response).to redirect_to new_user_session_path
          end
        end

        context "with a invalid parameters" do
          it "responds [302]" do
            put list_card_path(list, card), params: { card: invalid_params }
            expect(response).to have_http_status 302
          end
          it "responds [:found]" do
            put list_card_path(list, card), params: { card: invalid_params }
            expect(response).to have_http_status :found
          end
          it "updates no card" do
            expect do
              put list_card_path(list, card), params: { card: invalid_params }
            end.not_to change { Card.find(card.id).title }
          end
          it "redirects to sign_in" do
            put list_card_path(list, card), params: { card: invalid_params }
            expect(response).to redirect_to new_user_session_path
          end
        end
      end

      describe "#destroy" do
        it "responds [302]" do
          delete list_card_path(list, card)
          expect(response).to have_http_status 302
        end
        it "responds [:found]" do
          delete list_card_path(list, card)
          expect(response).to have_http_status :found
        end
        it "deletes a card" do
          post list_cards_path(list, card), params: { card: valid_params }
          expect do
            delete list_card_path(list, card)
          end.not_to change(Card, :count)
        end
        it "redirects to root_path" do
          post list_cards_path(list, card), params: { card: valid_params }
          delete list_card_path(list, card)
          expect(response).to redirect_to new_user_session_path
        end
      end
    end
  end
end
