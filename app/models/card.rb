class Card < ApplicationRecord
  belongs_to :list
  validates :title, length: { in: 1..20 }
end
