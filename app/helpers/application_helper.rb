module ApplicationHelper
  def make_title(given_title)
    if given_title.empty?
      "Tasklist"
    else
      given_title
    end
  end
end
