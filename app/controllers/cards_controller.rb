class CardsController < ApplicationController
  before_action :set_card, only: %i(show edit update destroy)
  before_action :set_list, only: %i(new create)

  def new
    @card = Card.new
  end

  def create
    @card = Card.new(card_params)
    if @card.save
      redirect_to :root
    else
      render :new
    end
  end

  def show
    redirect_to_root_by_nil_of(@card)
  end

  def edit
    redirect_to_root_by_nil_of(@card)
  end

  def update
    if @card.update_attributes(card_params)
      redirect_to :root
    else
      render :edit
    end
  end

  def destroy
    @card.destroy
    redirect_to :root
  end

  private

  def card_params
    params.require(:card).permit(:title, :memo, :list_id)
  end

  def set_card
    @card = set_(Card, :id)
  end

  def set_list
    @list = set_(List, :list_id)
  end
end
