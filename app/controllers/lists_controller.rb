class ListsController < ApplicationController
  before_action :set_list, only: %i(edit update destroy)

  def new
    @list = List.new
  end

  def create
    @list = List.new(list_params)
    if @list.save
      redirect_to :root
    else
      render :new
    end
  end

  def edit
    redirect_to_root_by_nil_of(@list)
  end

  def update
    if @list.update_attributes(list_params)
      redirect_to :root
    else
      render :edit
    end
  end

  def destroy
    if @list.destroy
      redirect_to :root
    else
      render :edit
    end
  end

  private

  def list_params
    params.require(:list).permit(:title).merge(user: current_user)
  end

  def set_list
    @list = set_(List, :id)
  end
end
