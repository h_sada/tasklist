class TopController < ApplicationController
  def index
    @lists = List.includes(:cards).where(user: current_user).order(created_at: :asc)
  end
end
