Rails.application.routes.draw do
  root                        to: "top#index"
  resources :top,           only: :index
  resources :lists,       except: %i(index, show) do
    resources :cards,     except: :index
  end
  devise_for :users
end
